#include<stdio.h>
#include<math.h>
#include<stdlib.h>

/*void snap(double* sx,double* sy,double slope,double Yoffset,double e,double n){
    //a=slope b=-1 from y=slope*x+c slope*x-y+c=0
    *sx=-((-e-slope*n)-slope*Yoffset)/(slope*slope+1);
    *sy=(slope*(e+slope*n)+Yoffset)/(slope*slope+1);
}*/
double snap(double m,double b,double e,double n){
    //a=slope b=-1 from y=slope*x+c slope*x-y+c=0
	/*double sx,sy;
	    sx=-((-e-slope*n)-slope*Yoffset)/(slope*slope+1);
    	sy=(slope*(e+slope*n)+Yoffset)/(slope*slope+1);*/

    double sx = (m * n + e - m * b) / (m * m + 1);
    double sy = (m * m * n + m * e + b) / (m * m + 1);
	return sqrt(sx*sx+sy*sy);
}
int main(){
    double px1,px2,py1,py2,e,n,sx,sy;
    while(1){
    scanf("%lf %lf %lf %lf %lf %lf",&px1,&py1,&px2,&py2,&e,&n);

    double slope=(py2-py1)/(px2-px1);
    double Yoffset=py2-slope*px2;
    printf("\n%.4f\n",snap(slope,Yoffset,e,n));
    return 0;
}
}
