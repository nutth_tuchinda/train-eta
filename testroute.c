#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
double lat[30],lon[30],km[30];
int currentIndex;
int tableLength;
double ER=6371;
void latlonToXYZ(double lat,double lon,double* x,double* y,double* z){
  lat=lat/180*M_PI;
  lon=lon/180*M_PI;
  *x=ER*cos(lat)*cos(lon);
  *y=ER*cos(lat)*sin(lon);
  *z=ER*sin(lat);
}
double distance3d(double x1,double y1,double z1,double x2,double y2,double z2){
  return (x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)+(z1-z2)*(z1-z2);
}
double distance2d(double x1,double y1,double x2,double y2){
  //printf("<<<<<<< distance calculated with\nx1= %.4f x2= %.4f\ny1=%.4f y2=%.4f\n= %.4f  >>>>>>>>>",x1,x2,y1,y2,(x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
  return (x1-x2)*(x1-x2)+(y1-y2)*(y1-y2);}
double sx=0,sy=0;
void snap(double m,double b,double e,double n){
    //a=slope b=-1 from y=slope*x+c slope*x-y+c=0
	/*double sx,sy;
	    sx=-((-e-slope*n)-slope*Yoffset)/(slope*slope+1);
    	sy=(slope*(e+slope*n)+Yoffset)/(slope*slope+1);*/

    sx = (m * n + e - m * b) / (m * m + 1);
    sy = (m * m * n + m * e + b) / (m * m + 1);
}
double findKM(double xn,double yn,int currentIndex){
  int i,j;
  double dist[tableLength];
  for(i=0;i<tableLength;i++){
    double x,y,z;
    dist[i]=sqrt(distance2d(lat[i],lon[i],xn,yn));
    printf("dist = %.4f\n",dist[i]);
    if(i>1){
      if((dist[i-2]>dist[i-1]&&dist[i]>dist[i-1]))
        if(dist[i-2]>dist[i]){
          printf("\n%.4f   %.4f\n",dist[i-2],dist[i]);
          printf("\nyayy1\n");
        	double slope=(lon[i-1]-lon[i])/(lat[i-1]-lat[i]); 
		double Yoffset=lon[i-1]-slope*lat[i-1]; 
    printf("slope = %.4f Yoffset = %.4f ",slope,Yoffset);
		snap(slope,Yoffset,xn,yn);
    return sqrt(distance2d(sx,sy,lat[i-1],lon[i-1]))/1000+km[i-1];
	  	
	}
        else{
          printf("\nyay2\n");
		double slope=(lon[i-2]-lon[i-1]/lat[i-2]-lat[i-1]);
		double Yoffset=lon[i-1]-slope*lat[i-1];
    snap(slope,Yoffset,xn,yn);
          	return sqrt(distance2d(sx,sy,lat[i-2],lon[i-2]))/1000+km[i-2];
	
	}
      if(i==tableLength-1&&dist[i]<dist[i-1]){
        printf("\nyay3\n");
        	double slope=(lon[i-1]-lon[i])/(lat[i-1]-lat[i]); 
		double Yoffset=lon[i-1]-slope*lat[i-1]; 
    snap(slope,Yoffset,xn,yn);
		return sqrt(distance2d(sx,sy,lat[i-1],lon[i-1]))/1000+km[i-1];}
    }
  }
  printf("\nyayy4\n");
  i=0;
        	double slope=(lon[i+1]-lon[i])/(lat[i+1]-lat[i]); 
		double Yoffset=lon[i+1]-slope*lat[i+1]; 
    snap(slope,Yoffset,xn,yn);
		
    printf ("%.4f %.4f\n",slope,Yoffset);
    return sqrt(distance2d(sx,sy,lat[i],lon[i]))/1000+km[i];
}
void getSDsettingout(FILE* n1){
  int i,j;
  char a[100];
  for(i=0;!feof(n1);i++){
    fscanf(n1,"%[^\n]%*c",a);
    //printf("%s\n",a);
    char i1[10],i2[10],i3[10];
    sscanf(a,"%s %s %s",i1,i2,i3);
    lat[i]=strtod(i2,NULL);
    lon[i]=strtod(i3,NULL);
    km[i]=strtod(i1,NULL);
  }
  tableLength=i;
}
int main(){
    FILE *n1=fopen("testroute.txt","r");
    getSDsettingout(n1);
    int i=0;
    for(i=0;i<tableLength;i++)
    printf("%.5f %.5f %.2f\n",lat[i],lon[i],km[i]);
    printf("%d\n",tableLength);
    while(1){
      double lt,lot,x,y,z;
      scanf("%lf %lf",&lt,&lot);
      printf("dist = %.4f\n",findKM(lt,lot,0));
    }
    return 0;
}
