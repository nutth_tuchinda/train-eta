const int ED = 5.5 ; //Estimated distance between Sensor A and Sensor B
unsigned long time_A ; 
unsigned long time_B ;
double duration;
const int PIRpinA = 2;
const int PIRpinB = 3;
const int ledpin = 11;
long i = 0;
int Indicate_A = 0;
int Indicate_B = 0 ;
void setup() {
  // put your setup code here, to run once:
  pinMode ( PIRpinA, INPUT );
  pinMode ( PIRpinB, INPUT );
  pinMode ( ledpin , OUTPUT );
  Serial.begin(115200);
  attachInterrupt(digitalPinToInterrupt(PIRpinA),ISR_A,FALLING);
  attachInterrupt(digitalPinToInterrupt(PIRpinB),ISR_B,FALLING);
  
}
void ISR_A (){      //Interrupting  Service Routine Sensor A
  Indicate_A = 1 ;
  time_A = micros();
}
void ISR_B (){       //Interrupting  Service Routine Sensor B
  if(Indicate_A == 1){
  Indicate_B = 1;
  time_B = micros();
  }
  else {
    Indicate_B = 0 ;
    }
}

void loop() {
  // put your main code here, to run repeatedly:
  bool Trainarrive = false;
  if(Indicate_A == 1 && Indicate_B == 1){
      
      unsigned long ET = time_B - time_A ; // Calculate Esitimate time 
      duration = (double) ET / 1000000;   //Convert microsecond to seceond
      if( duration < 3 && duration > 0.003){          // set the limition of duration must be less than 3 seconds.
        Trainarrive = true;
      }
  } 
  if( Trainarrive == true ){  
    Serial.println(time_A);
    Serial.println(time_B);
    
    Indicate_A = 0 ;    //Set Indicate A and B to Zero
    Indicate_B = 0 ;
    time_A = 0;
    time_B = 0;
    double Velocity = (double) ED /duration ; //Calculate Average speed in (m/s) from 1 meter Distance of 2 PIR sensor 
    double Velocity_kmhr = Velocity * 18 / 5;
    //Send a Message to Level Crossing
    Serial.println("         Train arriving          ");
    Serial.print("Velocity : ");
    Serial.print(Velocity,2);
    Serial.print(" m/s    or  ");
    Serial.print(Velocity_kmhr,2);
    Serial.println(" km/hr ");
    Serial.print("Duration : ");
    Serial.print(duration,2);
    Serial.println(" sec ");
    Serial.println("    --------------------------    ");
    
  }
}
