#include<stdio.h>
#include<stdlib.h>
#include<math.h>
double kmu[40],kmd[40],spdu1[40],etau2[40],spdu2[40],etad1[40],spdd1[40],etau1[40],spdd2[40],etad2[40];
int tableLength=0;
double kmmax=0,kmmin=9999;
void getSDup(){

  FILE *n1;
  n1=fopen("up.txt","r");
  int i,j;
  for(i=0;!feof(n1);i++){
    char a[100]="";
    /*for(j=0;;j++){
      a[j]=n1.read();
      if(a[j]=='\n'){
      a[j]='\0';
      break;
      }
    }*/
    fscanf(n1,"%*c %99[^\n]",a);
    //printf("%s\n",a);
    //Serial.println(a);
    char i1[20],i2[20],i3[20],i4[20],i5[20];
    sscanf(a,"%*c %s %*s %s %s %s %s",i1,i2,i3,i4,i5);
    kmu[i]=strtod(i1,NULL);
    spdu1[i]=strtod(i2,NULL);
    etau1[i]=strtod(i3,NULL);
    spdu2[i]=strtod(i4,NULL);
    etau2[i]=strtod(i5,NULL);
    printf("%.3f %.3f %.3f %.3f %.3f\n",kmu[i],spdu1[i],etau1[i],spdu2[i],etau2[i]);
  }
  tableLength=i;
  fclose(n1);
}

double getETAup(double km,double spd){
  if(km>kmmax)km=kmmax-0.001;
  if(km<kmmin)km=kmmin+0.001;
  int i=0,go=0;
  double range;
  for(i=0;i<tableLength-1;i++){
    range = kmu[i];
    range+=kmu[i+1];
    range/=2;
    if(km>=kmu[i]&&km<=range){
      go=i;
      break;
    }
    if(km<=kmu[i+1]&&km>=range){
      go=i+1;
      break;
    }
  }
  if(spdu1[go]>spdu2[go]){
    if(spd>spdu1[go])
      return etau1[go];
    if(spd<spdu2[go])
      return etau2[go];
  }
  else{
    if(spd>spdu2[go])
      return etau2[go];
    if(spd<spdu1[go])
      return etau1[go];
  }
  if(spd>(spdu1[go]+spdu2[go])/2)
    return etau1[go];
  else
    return etau2[go];
}
getETAdown(double km,double spd){
  int i=0,Destinate=0;
  if(km>kmmax)km=kmmax-0.001;
  if(km<kmmin)km=kmmin+0.001;
  for(i=0;i<tableLength-1;i++){
    double range=kmd[i]+kmd[i+1];
    range/=2;
    if(km>=kmd[i+1]&&km<=range){
      Destinate=i+1;
      break;
    }
    if(km<=kmd[i]&&km>=range){
      Destinate=i;
      break;
    }
  }
  if(spdd1[Destinate]>spdd2[Destinate]){
  	if(spd>spdd1[Destinate])
	return etad1[Destinate];
	if(spd<spdd2[Destinate])
	return etad2[Destinate];
  }
  else{
	if(spd>spdd2[Destinate])
	return etad2[Destinate];
	if(spd<spdd1[Destinate])
	return etad1[Destinate];
  }
  if(spd>(spdd1[Destinate]+spdd2[Destinate])/2)
  return etad1[Destinate];
  else
  return etad2[Destinate];
  
}
void getSDdown(){
  FILE *n1;
  n1=fopen("down.txt","r");  
int i,j;
  for(i=0;!feof(n1);i++){
    char a[100]="";
    /*for(j=0;;j++){
      a[j]=n1.read();
      if(a[j]=='\n'){
      a[j]='\0';
      break;
      }
    }*/
    fscanf(n1,"%*c %99[^\n]",a);
    //Serial.println(a);
    char i1[20],i2[20],i3[20],i4[20],i5[20];
    sscanf(a,"%*c %s %*s %s %s %s %s",i1,i2,i3,i4,i5);
    kmd[i]=strtod(i1,NULL);
    spdd1[i]=strtod(i2,NULL);
    etad1[i]=strtod(i3,NULL);
    spdd2[i]=strtod(i4,NULL);
    etad2[i]=strtod(i5,NULL);
  }
  tableLength=i;	
  fclose(n1);
}
int main(){
	getSDup();
	getSDdown();
	int i=0;        
	for(i=0;i<tableLength;i++){
		if(kmu[i]>kmmax)
		kmmax=kmu[i];
		if(kmu[i]<kmmin)
		kmmin=kmu[i];	
	}
	printf("kmmax = %.3f kmmin = %.3f",kmmax,kmmin);
	while(1){
		double km,spd;
		int direct;
		scanf("%lf %lf %d",&km,&spd,&direct);
		printf("\n");
double eta=0;		
if(direct==1)
  eta=getETAup(km,spd);
  if(direct==-1)
  eta=getETAdown(km,spd);
printf("%.3f",eta);
	}
	return 0;
}
