//#include "TEE_UC20.h"
#include <SPI.h>
#include <SD.h>
#include <stdlib.h>
#include <math.h>
#include "TEE_UC20.h"
//#include "SoftwareSerial.h"
//#include <AltSoftSerial.h>
#include "call.h"
#include "sms.h"
#include "internet.h"
#include "File.h"
#include "http.h"
//#include <Progmem.h>
INTERNET net;
UC_FILE file;
HTTP http;
const String publicKey = "EJ4YaAgqzqH2R4KKDDL3";
const String privateKey = "dqno6m9d2dUwK2dd88aZ";
const byte NUM_FIELDS = 9;
const String fieldNames[NUM_FIELDS] = {"date", "time", "id","lat","lon","dist","spd","dir","rr"};
String fieldData[NUM_FIELDS]={"010816","113847.000","1","13.745272","100.535820","2.376464","5.800000","1","0.01"};
//SIM TRUE  internet
#define APN "internet"
#define USER ""
#define PASS ""

//#include "call.h"
//#include "sms.h"
const int PIRpin = 13;
const int ledpin = 12;
int limit;
//CALL call;
//SMS sms;
//String phone_number = "0816290145";
File n1;
double rr=0;
double ER=6371;
double lat[20];
double lon[20];
double km[20];
int tableLength=0;
char date[20],time[20],dateC[20],timeC[20];
int md[12]={31,28,31,30,31,30,31,31,30,31,30,31};
void ZoneChange(){
    int day=(date[0]-48)*10+date[1]-48;
    int month=(date[2]-48)*10+date[3]-48;
    int year=(date[4]-48)*10+date[5]-48;
    int hour=(time[0]-48)*10+time[1]-48;
    int minute=(time[2]-48)*10+time[3]-48;
    int second=(time[4]-48)*10+time[5]-48;
    hour+=7;
    if(hour>=24){
        hour-=24;
        day++;
        if(year%4==0&&!(year%400!=0&&year%100==0))
            md[1]=29;
        else
            md[1]=28;
        if(day>md[month-1]){
            day=1;
            month++;
            if(month>12){
                month=1;
                year++;
            }
        }
    }
    dateC[0]=day/10+48;
    dateC[1]=day-(day/10)*10+48;
    dateC[2]=month/10+48;
    dateC[3]=month-(month/10)*10+48;
    dateC[4]=year/10+48;
    dateC[5]=(year-(year/10)*10)+48;
    dateC[6]='\0';
    timeC[0]=hour/10+48;
    timeC[1]=hour-(hour/10)*10+48;
    timeC[2]=minute/10+48;
    timeC[3]=minute-(minute/10)*10+48;
    timeC[4]=second/10+48;
    timeC[5]=second-(second/10)*10+48;
    timeC[6]='.';
    timeC[7]=48;
    timeC[8]=48;
    timeC[9]=48;
    timeC[10]='\0';
}
double bearing(double lat1,double lon1,double lat2,double lon2){
  lat1/=180/M_PI;
  lon1/=180/M_PI;
  lat2/=180/M_PI;
  lon2/=180/M_PI;
  double y=sin(lon2-lon1)*cos(lat2);
  double x=cos(lat1)*sin(lat2)-sin(lat1)*cos(lat2)*cos(lon2-lon1);
  return atan2(y,x);
}
void debug(String data)
{
  Serial.println(data);
}
double distL(double lat1,double lon1,double lat2,double lon2){
  lat1/=180/M_PI;
  lon1/=180/M_PI;
  lat2/=180/M_PI;
  lon2/=180/M_PI;
  double a=sin((lat2-lat1)/2)*sin((lat2-lat1)/2)+cos(lat1)*cos(lat2)*sin((lon2-lon1)/2)*sin((lon2-lon1)/2);
  double c=2*atan2(sqrt(a),sqrt(1-a));
  return ER*c;
}
void collectapiframe (byte *collect){       // Output : API frame and lenght
  //Serial3.flush();
  bool complete = false ;
  while(!complete){
    Serial3.flush();
    if(Serial3.available()>10) { 
     byte inbyte = Serial3.read();
     delay(1);
     byte inbyte1 = Serial3.read();
     delay(1);
      if(inbyte == 0x7e && inbyte1==0x00){ 
        byte inbyte2 = Serial3.read();
        delay(1);
        limit = (int)inbyte1 + (int)inbyte2;
        collect[0] = inbyte ;
        collect[1] = inbyte1  ;
        collect[2] = inbyte2  ;
      }
      for(int i = 3 ; i <= limit+3 ; i++){         
        collect[i] = Serial3.read();
        delay(1);
      }
      for(int i = 0 ; i <= limit + 3 ; i++){
        //Serial.print(collect[i],HEX);
        //Serial.print(",");                                      
      }
      //Serial.print("    ");
      complete = true;
    }
  }
}
void TransmitRequest(byte *data,int framelength,byte *address,int addresslength,int frameid){      // Frame type 0x10
  int framelength1 = framelength + 14;
  long checksum = 0;
  Serial3.write(0x7E);
  Serial3.write(0x00);                         //MSB Length
  Serial3.write(framelength1);                  //LSB Length
  Serial3.write(0x10);                         //Frametype 0x10
  Serial3.write(frameid);
  for(int i = 0 ; i < addresslength ; i++){        // Address 64 bit
    Serial3.write(address[i]);
    checksum = checksum + address[i];
  }
  Serial3.write(0xFF);                        // Address 16 bit Set to 0xFFFE if unknown
  Serial3.write(0xFE);
  Serial3.write(0x00);                        // Broadcast radius
  Serial3.write(0x00);                        // Options
  for(int i = 0 ; i < framelength ; i++){     // RF data
    Serial3.write(data[i]);
    checksum = checksum + data[i];
  }
  checksum = checksum + 0x10 + frameid + 0xFF + 0xFE ;
  checksum = 0xFF - (checksum & 0xFF);
  Serial3.write(checksum);                    // Checksum byte
} 
bool confirmframe(byte *collect,int frameid){
  //Serial.print(collect[4],HEX); //Serial.print(frameid,HEX);
  bool compare;
  if ( collect[4] == frameid && collect[8] == 0 ){
    compare = true;
  }
  else{
    compare = false;
  } 
  return compare;
}
int frameid=1;
byte collect[] = {} ;
//float total = 0 , success = 0  , fail=0;
//byte data[] = { 'Z', 'I', 'G', 'B', 'E', 'E', 'T', 'E', 'S', 'T'};    //  Data to send to Target
byte address[] = {0x00,0x13,0xA2,0x00,0x40,0xDA,0x50,0xF8};    //  Set Target address //train { 0x00, 0x13, 0xA2, 0x00, 0x40, 0xF1, 0x65, 0xC4}
//byte address[8] =  {0x00,0x13,0xA2,0x00,0x40,0xE5,0xCE,0xC0};
void sentData(double dist,double spd,int directState){
  //Serial.println(t);
  //Serial.println(date);
  int limit;
  char Bdist[sizeof(dist)],Bspd[sizeof(spd)],BdirectState[sizeof(directState)];
  memcpy(&Bdist,&dist,sizeof(dist));
  memcpy(&Bspd,&spd,sizeof(spd));
  memcpy(&BdirectState,&directState,sizeof(directState));
  //memcpy(&dat,&dataR,sizeof(dataR));
  byte data[26];
  int i=0;
  for(i=0;i<4;i++)
  data[i]=Bdist[i];
  for(i=0;i<4;i++)
  data[i+4]=Bspd[i];
  for(i=0;i<2;i++)
  data[i+8]=BdirectState[i];
  for(i=0;i<10;i++)
  data[i+10]=timeC[i];
  for(i=0;i<6;i++)
  data[i+20]=dateC[i];
  //Serial.println(dateC[5]);
  //Serial.println();
  //for(i=0;i<10;i++){
  ////Serial.print(" ");//Serial.print(data[i]);}
  ////Serial.println();
  /*double check;
  memcpy(&check,&data,sizeof(check));
  //Serial.print("check = ");
  //Serial.println(check);*/
  ////Serial.println(sizeof(dat));
  TransmitRequest(data,sizeof(data), address,sizeof(address),frameid);
  //Serial.println("Transmited");
  //collectapiframe(collect);
  ////Serial.println("Collected API frame");
  frameid++;
}
void getData(double* lat,double* lon,double* spd,int* state){
  //n1=SD.open("1.TXT");
  int check=0;
  *state=0;
  int state1=0,state2=0,state3=0;
  do{
    char l[1000]="";
    int i=0;
    do{
      if(Serial1.available()>0){
      l[i]=Serial1.read();
      i++;}
      if(l[i-1]==10){l[i-1]=0;break;}
    }while(l[i-1]!=10);
    //fscanf(n1,"%s\n",l);
    ////Serial.println(l);
    if(((l[1]=='G'&&l[2]=='P'&&l[3]=='V'&&l[4]=='T')&&state1==0)||((l[1]='G'&&l[2]=='N'&&l[3]=='G'&&l[4]=='L')&&state2==0)||((l[1]='G'&&l[2]=='N'&&l[3]=='R'&&l[4]=='M')&&state3==0)){
      ////Serial.println("found");
      check=1;int k=0;
      if(l[3]=='V') //$GNGLL,,,,,073120.00,V,N*53
      state1=1;
      if(l[3]=='G')
      state2=1;
      if(l[3]=='R')
      state3=1;
      if(l[3]=='G'&&l[21]!='V'){//position
        int k=0;
        for(k=0;k<strlen(l);k++)
          if(l[k]==',')l[k]=' ';
        char a[20],b[20];
        sscanf(l,"%*s %s %*c %s %*c %s %*s",a,b,time);
          double c=strtod(a,0);
          double d=strtod(b,0);
          double e=(int)c/100;
          double f=(int)d/100;
          c-=e*100;
          d-=f*100;
          *lat=c/60+e;
          *lon=d/60+f;
      }
      if(l[21]=='V'){
        *lat=0;*lon=0;
      }
      if(l[3]=='R'){//position
        int k=0;
        for(k=0;k<strlen(l);k++)
          if(l[k]==',')l[k]=' ';
        char a[20],b[20];
        sscanf(l,"%*s %*s %*c %*s %*c %*s %*c %*s %*s %s",date);
      }
      if(l[18]=='V'){
        *lat=0;*lon=0;
      }
      if(l[3]=='V'){
        int k=0;
        for(k=0;k<strlen(l);k++)
          if(l[k]==',')l[k]=' ';
        char a[20];
        sscanf(l,"%*s %*s %*c %*s %*c %*s %*c %s",a);
        *spd = strtod(a,NULL);
      }
      /*for(k=0;k<strlen(l)-1;k++)
      if(l[k]!='V'&&){
        for(int j=0;j<i-1;j++){
          if(l[j]==',')
            l[j]=' ';
        }
          //Serial.println(l);
          char a[100],b[100],c[100],d[100];
          sscanf(l,"%*s %s %*c %s %*c %s %*c %s",a,b,c,d);
          //Serial.println(a);
          *lat=strtod(b,NULL);
          *lon=strtod(c,NULL);
          *t=strtod(a,NULL);
          *spd=strtod(d,NULL);
        }*/
      }
    if(state1!=0&&state2!=0&&state3!=0)break;
  }while(1);
  //n1.close();
}

void latlonToXYZ(double lat,double lon,double* x,double* y,double* z){
  lat=lat/180*M_PI;
  lon=lon/180*M_PI;
  *x=ER*cos(lat)*cos(lon);
  *y=ER*cos(lat)*sin(lon);
  *z=ER*sin(lat);
}
double distance3d(double x1,double y1,double z1,double x2,double y2,double z2){
  return (x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)+(z1-z2)*(z1-z2);
}

double findKM(double xn,double yn,double zn,int currentIndex){
  int i,j;
  double dist[tableLength];
  for(i=0;i<tableLength;i++){
    double x,y,z;
    latlonToXYZ(lat[i],lon[i],&x,&y,&z);
    dist[i]=distance3d(x,y,z,xn,yn,zn);
    printf("dist = %.4f\n",sqrt(dist[i]));
    if(i>1){
      if((dist[i-2]>dist[i-1]&&dist[i]>dist[i-1]))
        if(dist[i-2]>dist[i])
          return sqrt(dist[i-1])+km[i-1];
        else
          return sqrt(dist[i-2])+km[i-2];
      if(i==tableLength-1&&dist[i]<dist[i-1])
        return sqrt(dist[i-1])+km[i-1];
    }
  }
  
    return sqrt(dist[0])+km[0];
}
double findKMLL(double lat1,double lon1,int currentIndex){
  int i,j;
  double dist[tableLength];
  for(i=0;i<tableLength;i++){
    dist[i]=distL(lat[i],lon[i],lat1,lon1);
    printf("dist = %.4f\n",sqrt(dist[i]));
    if(i>1){
      if((dist[i-2]>dist[i-1]&&dist[i]>dist[i-1])){
        if(dist[i-2]>dist[i]){
          rr=distL(lat[i-1],lon[i-1],lat1,lon1)*sin(bearing(lat[i],lon[i],lat[i-1],lon[i-1])-bearing(lat1,lon1,lat[i-1],lon[i-1]));
          return dist[i-1]*cos(bearing(lat[i],lon[i],lat[i-1],lon[i-1])-bearing(lat1,lon1,lat[i-1],lon[i-1]))+km[i-1];
          }
        else{
          rr=distL(lat[i-2],lon[i-2],lat1,lon1)*sin(bearing(lat[i-2],lon[i-2],lat[i-1],lon[i-1])-bearing(lat[i-2],lon[i-2],lat1,lon1));
          return dist[i-2]*cos(bearing(lat[i-2],lon[i-2],lat[i-1],lon[i-1])-bearing(lat[i-2],lon[i-2],lat1,lon1))+km[i-2];
          }
      }
      if(i==tableLength-1&&dist[i]<dist[i-1]){
        
          rr=distL(lat[i-1],lon[i-1],lat1,lon1)*sin(bearing(lat[i],lon[i],lat[i-1],lon[i-1])-bearing(lat1,lon1,lat[i-1],lon[i-1]));
        return dist[i-1]*cos(bearing(lat[i],lon[i],lat[i-1],lon[i-1])-bearing(lat1,lon1,lat[i-1],lon[i-1]))+km[i-1];
        
      }
    }
  }
    i=1;
          rr=distL(lat[i-1],lon[i-1],lat1,lon1)*sin(bearing(lat[i],lon[i],lat[i-1],lon[i-1])-bearing(lat1,lon1,lat[i-1],lon[i-1]));
    return dist[0]*cos(bearing(lat[0],lon[0],lat[1],lon[1])-bearing(lat[0],lon[0],lat1,lon1))+km[0];
}

void getSDsettingout(){
  n1=SD.open("test.txt");
  int i,j;
  for(i=0;n1.available();i++){
    char a[100]="";
    for(j=0;;j++){
      a[j]=n1.read();
      if(a[j]=='\n'){
      a[j]='\0';
      break;
      }
    }
    ////Serial.println(a);
    char i1[20],i2[20],i3[20];
    sscanf(a,"%*c %s %s %s",i1,i2,i3);
    lat[i]=strtod(i2,NULL);
    lon[i]=strtod(i3,NULL);
    km[i]=strtod(i1,NULL);
  }
  tableLength=i;
  //Serial.println("get com");
}
String templat="/input/"+(String)publicKey+"?private_key="+(String)privateKey;
  
void setup() {
  Serial.begin(9600);
  Serial1.begin(9600);
  gsm.begin(&Serial2,9600);
  //gsm.Event_debug = debug;
  Serial.println(F("UC20"));
  //gsm.PowerOn();
  //while(gsm.WaitReady()){}
  Serial.print(F("GetOperator --> "));
  Serial.println(gsm.GetOperator());
  Serial.print(F("SignalQuality --> "));
  Serial.println(gsm.SignalQuality());
  Serial.println(F("Disconnect net"));
  net.DisConnect();
  Serial.println(F("Set APN and Password"));
  net.Configure(APN,USER,PASS);
  Serial.println(F("Connect net"));
  net.Connect();
  Serial.println(F("Show My IP"));
  Serial.println(net.GetIP());
  Serial.println(F("Start HTTP"));
  http.begin(1);
  Serial.println(F("Send HTTP POST"));
  Serial3.begin(9600);
  if(!SD.begin(53)){
  Serial.println("failed");
  while(1)
  delay(500);
  }getSDsettingout();
  int i=0;
  for(i=0;i<tableLength;i++){
    Serial.print(km[i]);
    Serial.print(" ");
  }
  Serial.println();
  //pinMode ( PIRpin, INPUT_PULLUP );
  //pinMode ( ledpin , OUTPUT );
  pinMode(13, OUTPUT);
  
}
int i=0,directState=0,queCount=0;
double que[15],max=10000,min=0;
void loop() {
  //Serial.print("freeMemory()=");
  //Serial.println(freeMemory());
  double latn=0,lonn=0,spd=0;
  double x=0,y=0,z=0;
  int state=0;
  int currentIndex=0;
  getData(&latn,&lonn,&spd,&state);
  digitalWrite(13, HIGH);
    Serial.print("Lat = ");
    Serial.print(latn);
    Serial.print(" Lon = ");
    Serial.print(lonn);
    //Serial.print(" Speed = ");
    //Serial.print(spd);
    //Serial.println(" km/hr");
    if(latn>0&&lonn>0){
      latlonToXYZ(latn,lonn,&x,&y,&z);
      double dist=findKMLL(latn,lonn,currentIndex);
      Serial.println(dist);
      if(queCount<15){
        que[queCount]=dist;
        queCount++;
      }
      if(queCount==15){
        int ca=0;
        min=10000;max=0;
        for(ca=0;ca<queCount-1;ca++){
          que[ca]=que[ca+1];
          if(que[ca]>max)
          max=que[ca];
          if(que[ca]<min)
          min=que[ca];
          }
        que[14]=dist;
        if(dist>max)
        max=dist;
        if(dist<min)
        min=dist;
        if(max-min>0.09){
          if(que[queCount-1]-que[0]>0)
            directState=1;
          else
            directState=-1;
        }
      }
      //Serial.print(i);
      //Serial.print(" ");
      //Serial.println(dist);
      ZoneChange();
      time[10]='\0';
      char fn[14];
      int i=0;
      for(i=0;i<6;i++)
      fn[i]=date[i];
      fn[6]='O';
      fn[7]='B';
      fn[8]='.';
      fn[9]='t';
      fn[10]='x';
      fn[11]='t';
      fn[12]='\0';
      n1=SD.open(fn,FILE_WRITE);
      n1.print(dateC);
      fieldData[0]=(String)dateC;
      fieldData[1]=(String)timeC;
      fieldData[2]="1";
      fieldData[3]=String(latn,8);
      fieldData[4]=String(lonn,8);
      fieldData[5]=String(dist,8);
      fieldData[6]=String(spd,8);
      fieldData[7]=(String)directState;
      fieldData[8]=String(rr,8);
      n1.print(" ");
      n1.print(timeC);
      n1.print(" ");
      n1.print("1");
      n1.print(" ");
      n1.print(latn,6);
      n1.print(" ");
      n1.print(lonn,6);
      n1.print(" ");
      n1.print(dist,6);
      n1.print(" ");
      n1.print(spd,6);
      n1.print(" ");
      n1.print(directState);
      n1.print(" ");
      n1.println(rr);
      sentData(dist,spd,directState);
      n1.close();
      String a = templat;
      for(i=0;i<NUM_FIELDS;i++){
        a+="&";
        a+=(String)fieldNames[i];
        a+="=";
        a+=(String)fieldData[i];
        }
      String b="\n";
      a+=b;
      http.url("http://data.sparkfun.com"+a);
      digitalWrite(13, LOW);
      
  gsm.println("AT+QHTTPGET=80");
      digitalWrite(13, HIGH);
      /*if(dist>2.0&&state==0){
        sms.sentln("arrived at the destination");
        state=1;
      }*/
    }
}
