#include <Wire.h>
#include <Time.h>
#include <DS1307RTC.h>
#include <SPI.h>
#include <SD.h>
#include <stdlib.h>
#include <math.h>
#include "TEE_UC20.h"
#include "call.h"
#include "sms.h"
#include "internet.h"
#include "File.h"
#include "http.h"
//#include <Progmem.h>
INTERNET net;
UC_FILE file;
HTTP http;
const String publicKey = "wp06JDLmXKf4RvngwvnO";
const String privateKey = "wzp56yljDdSe0Agk9Ag5";
const byte NUM_FIELDS = 8;
const String fieldNames[NUM_FIELDS] = {"date", "time", "id","dist","spd","dir","pro","eta"};
String fieldData[NUM_FIELDS]={"010816","113847.000","1","13.745272","100.535820","2.376464","5.800000","1"};
//SIM TRUE  internet
#define APN "internet"
#define USER ""
#define PASS ""
double r1,r2,kmmax=0,kmmin=1000,distup[40],KiloD[40],spdu1[40],etau1[40],spdu2[40],etau2[40],spdd1[40],etad1[40],spdd2[40],etad2[40];
int tableLength=0;
File n1;
byte collect[100] = {}; //index i
byte address[8]={};    //index j
byte data[100]={};
byte dataold[100]={};
//byte getdata[100]={};
byte frametype;
long checksum = 0;
int limit = 0;
const int digitalpin = 2;
int profile=0;
void print2digits(int number) {
  if (number >= 0 && number < 10) {
    n1.write('0');
  }
  n1.print(number);
}
void print2digits1(int number) {
  if (number >= 0 && number < 10) {
    Serial.write('0');
  }
  Serial.print(number);
}
bool checksumbyte(byte *collect, int limit){
  checksum = 0;
  for(int i = 0; i < limit ; i++){
    checksum = (checksum & 0xFF) + (collect[i+3] & 0xFF);
    Serial.print(checksum,HEX);
    Serial.print(",");  
  }
  
    checksum = 0xFF - (checksum & 0xFF);
    Serial.println();
    Serial.println(checksum,HEX);
    Serial.println(collect[limit+3],HEX);
  if(checksum == collect[limit + 3]){
    return true;
  }
  if(checksum != collect[limit + 3]){
    return false;
  }
}
void collectapiframe (byte *collect){       // Output : API frame and lenght
  bool complete = true ; 
  while(complete){
    Serial3.flush();
    if(Serial3.available()>21) { 
     byte inbyte = Serial3.read();
     //Serial.print(inbyte);
     delay(1);
     byte inbyte1 = Serial3.read();
        delay(1);
      if(inbyte == 0x7e && inbyte1 == 0x00){
        byte inbyte2 = Serial3.read();
        delay(1);
        limit = (int)inbyte1 + (int)inbyte2;
        collect[0] = inbyte;
        collect[1] = inbyte1;
        collect[2] = inbyte2;
      }
      for(int i = 3 ; i <= limit+3 ; i++){         
        collect[i] = Serial3.read();
        delay(1);
      }
      for(int i = 0 ; i <= limit + 3 ; i++){
        Serial.print(collect[i],HEX);
        Serial.print(",");                                      
      }
      Serial.println();
        complete = false ;
    }
  }
}
void getaddress (byte *collect){                          //Input : API frame  Output : 64 bits Address and Frame type 
   for(int j=0 ; j <= 7 ;j++){                
        address[j]=collect[j+4];
    }                                     
   frametype = collect[3];                
}
bool compareaddress(byte *targetaddress){                //Check address  Input : Address  Output : Boolean
  
  bool compare = true;
  //byte targetaddress[10] ={0,0x13,0xA2,0,0x40,0xF1,0x65,0xC4,0x63,0x94};  //Set Target address
  int i = 0;
  compare = true;
  while(true){
    if(i>7){
      break;
    }
    if(targetaddress[i]!=address[i]){   
      compare = false;
      break;
    }
    i++;
  }
  return compare ;
}
void getdata (byte *collect,int limit) {           //Get Data   Output : Data

  if(frametype == 0x92){  // Type Data sample Rx Indicator
    for(int d = 15 ; d <= limit+2  ; d++){
      data[d-15] = collect[d];
    }
  }
  if(frametype == 0x90){ // Type Recieve packet
    for(int d = 15 ; d <= limit+2  ; d++){
      data[d-15] = collect[d];
    }
  }
  for(int d = limit-12; d < sizeof(data); d++){
    data[d]=0;
  }
}
bool comparedata(byte *data,byte *dataold){             //Compare data  Output : True or False
  int i = 0;
  bool compare = true;
  while(i < sizeof(data)){
    if ( data[i] != dataold[i] ){
      compare = false;
      break;
    }
    i++;
  }
  Serial.println(compare);
  return compare ;
}
//double getETAup(double km,double spd);
//double getETAdown(double km,double spd);
void getSDup(){
  n1=SD.open("up.txt");
  int i,j;
  for(i=0;n1.available();i++){
    char a[100]="";
    for(j=0;;j++){
      a[j]=n1.read();
      if(a[j]=='\n'){
      a[j]='\0';
      break;
      }
    }
    //Serial.println(a);
    char i1[20],i2[20],i3[20],i4[20],i5[20];
    sscanf(a,"%*c %s %*s %s %s %s %s",i1,i2,i3,i4,i5);
    distup[i]=strtod(i1,NULL);
    spdu1[i]=strtod(i2,NULL);
    etau1[i]=strtod(i3,NULL);
    spdu2[i]=strtod(i4,NULL);
    etau2[i]=strtod(i5,NULL);
  }
  tableLength=i;
  Serial.println("get com");
  n1.close();
}

void getSDdown(){
  n1=SD.open("down.txt");
  int i,j;
  for(i=0;n1.available();i++){
    char a[100]="";
    for(j=0;;j++){
      a[j]=n1.read();
      if(a[j]=='\n'){
      a[j]='\0';
      break;
      }
    }
    //Serial.println(a);
    char i1[20],i2[20],i3[20],i4[20],i5[20];
    sscanf(a,"%*c %s %*s %s %s %s %s",i1,i2,i3,i4,i5);
    KiloD[i]=strtod(i1,NULL);
    spdd1[i]=strtod(i2,NULL);
    etad1[i]=strtod(i3,NULL);
    spdd2[i]=strtod(i4,NULL);
    etad2[i]=strtod(i5,NULL);
  }
  tableLength=i;
  Serial.println("get com");
  n1.close();
}
double getETAu(double km,double spd){
  if(km>kmmax)km=kmmax-0.001;
  if(km<kmmin)km=kmmin+0.001;
  int i=0,go=0;
  for(i=0;i<tableLength-1;i++){
    r1 = distup[i];
    r1+=distup[i+1];
    r1/=2;
    if(km>=distup[i]&&km<=r1){
      go=i;
      break;
    }
    if(km<=distup[i+1]&&km>=r1){
      go=i+1;
      break;
    }
  }
  if(spdu1[go]>spdu2[go]){
    if(spd>spdu1[go]){
      profile=1;return etau1[go];}
    if(spd<spdu2[go]){
      profile=2;return etau2[go];}
  }
  else{
    if(spd>spdu2[go]){
      profile=1;return etau2[go];}
    if(spd<spdu1[go]){
      profile=2;return etau1[go];}
  }
  if(spd>(spdu1[go]+spdu2[go])/2){
    profile=1;return etau1[go];}
  else{
    profile=2;return etau2[go];}
}double deb(int i){
  return (KiloD[i]+KiloD[i+1])/2.0;
}
double aaa(double km,double spd){
  int i=0,go=0;
  if(km>kmmax)km=kmmax-0.001;
  if(km<kmmin)km=kmmin+0.001;
  for(i=0;i<tableLength-1;i++){
    r2=deb(i);
    if(km>=KiloD[i+1]&&km<=r2){
      go=i+1;
      break;
    }
    if(km<=KiloD[i]&&km>=r2){
      go=i;
      break;
    }
  }
  if(spdd1[go]>spdd2[go]){
    if(spd>spdd1[go]){
  profile=1;return etad1[go];}
  if(spd<spdd2[go]){
  profile=2;return etad2[go];}
  }
  else{
  if(spd>spdd2[go]){
  profile=1;return etad2[go];}
  if(spd<spdd1[go]){
  profile=2;return etad1[go];}
  }
  if(spd>(spdd1[go]+spdd2[go])/2){
  profile=1;return etad1[go];}
  else{
  profile=2;return etad2[go];}
  
}
char t[11]="164950.000",date[11]="010816";
int Zigbee = 0;
void getZigbee(double* km,double* spd,int* directState){
      collectapiframe(collect);
      Serial.println("collected");
      if(checksumbyte(collect,limit)){
        getaddress(collect);
        byte targetaddress1[8] ={0x00,0x13,0xA2,0x00,0x40,0xE5,0xCE,0xC0};     //Set the Targetaddress from train
        byte targetaddress2[8] = {0x00,0x13,0xA2,0x00,0x40,0xF1,0x65,0xC4};   // Set the Targetaddress from Wayside
        byte targetaddress3[8] = {0x00,0x13,0xA2,0x00,0x40,0x9F,0x40,0xF1};   // Set the Targetaddress from Wayside2
        if (compareaddress(targetaddress1)){      // Do this when API frame from Train
          Zigbee = 1 ;
        getdata(collect,limit);
        memcpy(km,&data[0],4);
      memcpy(spd,&data[4],4);
      memcpy(directState,&data[8],2);
      int i=0;
      for(i=0;i<10;i++)
      t[i]=data[i+10];
      for(i=0;i<6;i++)
      date[i]=data[i+20];
      t[10]='\0';
      date[6]='\0';
        }
        if(compareaddress(targetaddress2)){   // Do this when API frame from WAYSIDE
          Zigbee = 2;
        }
       
      if(compareaddress(targetaddress3)){     // Do this when API frame from WAYSIDE2
          Zigbee = 3 ;
      }
      }
}
String templat="/input/"+(String)publicKey+"?private_key="+(String)privateKey;
void setup(){
  Serial.begin(9600);
  pinMode(13, OUTPUT);
  gsm.begin(&Serial2,9600);
  Serial.println(F("UC20"));
  //gsm.PowerOn();
  //while(gsm.WaitReady()){}
  Serial.print(F("GetOperator --> "));
  Serial.println(gsm.GetOperator());
  Serial.print(F("SignalQuality --> "));
  Serial.println(gsm.SignalQuality());
  Serial.println(F("Disconnect net"));
  net.DisConnect();
  Serial.println(F("Set APN and Password"));
  net.Configure(APN,USER,PASS);
  Serial.println(F("Connect net"));
  net.Connect();
  Serial.println(F("Show My IP"));
  Serial.println(net.GetIP());
  Serial.println(F("Start HTTP"));
  http.begin(1);
  Serial.println(F("Send HTTP POST"));
  Serial3.begin(9600);
  if(!SD.begin(53)){
  Serial.println("failed");
  while(1)
  delay(500);
  }
  getSDup();
  getSDdown();
  int i=0;
  Serial.println("get up down com");
  for(i=0;i<tableLength;i++){
    if(distup[i]>kmmax)
    kmmax=distup[i];
    if(distup[i]<kmmin)
    kmmin=distup[i]; 
  }
  Serial.println("get max com");
  digitalWrite(13, HIGH);
}
void loop(){
  double km=0,spd=0,eta=0;
  int direct=0;
  getZigbee(&km,&spd,&direct);
  if(Zigbee == 1){   //Do this when API frame from train
  digitalWrite(13, LOW);
  Serial.println("getZigbeeCom");
  if(direct==1)
  eta=getETAu(km,spd);
  if(direct==-1)
  eta=aaa(km,spd);
  char fn[14];
  int i=0;
  for(i=0;i<6;i++)
  fn[i]=date[i];
  fn[6]='L';
  fn[7]='X';
  fn[8]='.';
  fn[9]='t';
  fn[10]='x';
  fn[11]='t';
  fn[12]='\0';
  n1=SD.open(fn,FILE_WRITE);
  fieldData[0]=(String)date;
  fieldData[1]=(String)t;
  fieldData[2]="1";
  fieldData[3]=String(km,8);
  fieldData[4]=String(spd,8);
  fieldData[5]=(String)direct;
  fieldData[6]=(String)profile;
  fieldData[7]=(String)eta;
  String a=templat;
  for(i=0;i<NUM_FIELDS;i++){
        a+="&";
        a+=(String)fieldNames[i];
        a+="=";
        a+=(String)fieldData[i];
        }
  http.url("http://data.sparkfun.com"+a);
  Serial.println(a);
  gsm.println("AT+QHTTPGET=80");
  n1.print(date);
  n1.print(" ");
  n1.print(t);
  n1.print(" ");
  n1.print("1 ");
  n1.print(km,6);
  n1.print(" ");
  n1.print(spd,6);
  n1.print(" ");
  n1.print(direct);
  n1.print(" ");
  n1.print(profile);
  n1.print(" ");
  n1.println(eta);
  Serial.print(km,6);
  Serial.print(" ");
  Serial.print(spd,6);
  Serial.print(" ");
  Serial.print(direct);
  Serial.print(" ");
  Serial.print(date);
  Serial.print(" ");
  Serial.print(t);
  Serial.print(" ");
  Serial.print(eta);
  Serial.print(" ");
  Serial.print(profile);
  Serial.print(" ");
  if(direct==0)
  Serial.println("Indeterminate");
  else Serial.println("");
  n1.close();
  digitalWrite(13, HIGH);
  }
  if(Zigbee == 2){                            // Do this when API frame from WAYSIDE 
    n1 = SD.open("W_status.txt",FILE_WRITE);
    tmElements_t tm;
    if (RTC.read(tm)) {
    n1.print("WAYSIDE Status : Ok, Time = ");
    print2digits(tm.Hour);
    n1.print(':');
    print2digits(tm.Minute);
    n1.print(':');
    print2digits(tm.Second);
    n1.print(", Date (D/M/Y) = ");
    n1.print(tm.Day);
    n1.print('/');
    n1.print(tm.Month);
    n1.print('/');
    n1.print(tmYearToCalendar(tm.Year));
    n1.println();
    n1.close();
  }
  if (RTC.read(tm)) {
    Serial.print("WAYSIDE Status : Ok, Time = ");
    print2digits1(tm.Hour);
    Serial.write(':');
    print2digits1(tm.Minute);
    Serial.write(':');
    print2digits1(tm.Second);
    Serial.print(", Date (D/M/Y) = ");
    Serial.print(tm.Day);
    Serial.write('/');
    Serial.print(tm.Month);
    Serial.write('/');
    Serial.print(tmYearToCalendar(tm.Year));
    Serial.println();
  } 
  }
  if(Zigbee == 3){                              // Do this when API frame from WAYSIDE2
    n1 = SD.open("W2_status.txt",FILE_WRITE);
    tmElements_t tm;
    if (RTC.read(tm)) {
    n1.print("WAYSIDE2 Status : Ok, Time = ");
    print2digits(tm.Hour);
    n1.print(':');
    print2digits(tm.Minute);
    n1.print(':');
    print2digits(tm.Second);
    n1.print(", Date (D/M/Y) = ");
    n1.print(tm.Day);
    n1.print('/');
    n1.print(tm.Month);
    n1.print('/');
    n1.print(tmYearToCalendar(tm.Year));
    n1.println();
    n1.close();
  }
  if (RTC.read(tm)) {
    Serial.print("WAYSIDE2 Status : Ok, Time = ");
    print2digits1(tm.Hour);
    Serial.write(':');
    print2digits1(tm.Minute);
    Serial.write(':');
    print2digits1(tm.Second);
    Serial.print(", Date (D/M/Y) = ");
    Serial.print(tm.Day);
    Serial.write('/');
    Serial.print(tm.Month);
    Serial.write('/');
    Serial.print(tmYearToCalendar(tm.Year));
    Serial.println();
  } 
  }
}

