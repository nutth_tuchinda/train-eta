#include<stdio.h>
#include<stdlib.h>
#include<math.h>
double atanh(double z){
  return 1/2*log((1+z)/(1-z));
}
double asinh(double z){
  return log(z+sqrt(1+z*z));
}
void latlontoUTM(double lat,double lon,int* N,int* E){
   //convert latlontoUTM
    double a=6378137,
    b=6356752.3142,
    f=0.003353,
    fi=298.257223,
    rm=6367435.679694,
    k0=0.9996,
    e=0.081819,
    n=0.001679,
    AA=6367449.145801,
    a1=8.377E-04,
    a2=7.609E-07,
    a3=1.198E-09,
    a4=2.429E-12,
    a5=5.712E-15,
    a6=1.480E-17,
    a7=4.108E-20,
    a8=1.200E-22,
    a9=3.647E-25,
    a10=1.156E-27,
    b1=8.377E-04,
    b2=5.906E-08,
    b3=1.673E-10,
    b4=2.165E-13,
    b5=3.788E-16,
    b6=7.237E-19,
    b7=1.493E-21,
    b8=3.254E-24,
    b9=7.391E-27,
    b10=1.738E-29;
    int LongZone=31+(int)(lon/6);
    double LongZoneCM=6*LongZone-183;
    double lonr=(lon-LongZoneCM)*M_PI/180;
    double latr=lat*M_PI/180;
    double alatr=fabs(latr);
    double sigma=sinh(e*atanh(e*tan(latr)/sqrt(1+tan(latr)*tan(latr))));
    double tau=tan(latr);
    double confLat=atan(tan(latr)*sqrt(1+sigma*sigma)-sigma*sqrt(1+tan(latr)*tan(latr)));
    double tauP=tan(confLat);
    double XIP=atan(tan(confLat)/cos(lonr));
    double ETAP=asinh(sin(lonr)/sqrt(tan(confLat)*tan(confLat)+cos(lonr)*cos(lonr)));
    double XI=XIP+a1*sin(2*XIP)*cosh(2*ETAP)+a2*sin(4*XIP)*cosh(4*ETAP)+a3*sin(6*XIP)*cosh(6*ETAP)+a4*sin(8*XIP)*cosh(8*ETAP)+a5*sin(10*XIP)*cosh(10*ETAP)+a6*sin(12*XIP)*cosh(12*ETAP);
    double ETA=ETAP+a1*cos(2*XIP)*sinh(2*ETAP)+a2*cos(4*XIP)*sinh(4*ETAP)+a3*cos(6*XIP)*sinh(6*ETAP)+a4*cos(8*XIP)*sinh(8*ETAP)+a5*cos(10*XIP)*sinh(10*ETAP)+a6*cos(12*XIP)*sinh(12*ETAP);
    double rawEast=k0*AA*ETA;
    double rawNorth=k0*AA*XI;
    double trueEast=rawEast+500000;
    *N=(int)trueEast;
    if(rawNorth>0)
    *E=(int)rawNorth;
    else
    *E=10000000+(int)rawNorth;
}
int main(){
	double lat,lon;
    int N,E;
    while(1){
        scanf("%lf %lf",&lat,&lon,N,E);
        latlontoUTM(lat,lon,&N,&E);
        printf("---  N =%d E = %d --- \n",N,E);
    }
    return 0;
}
