#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
double rr=0,lat[30],lon[30],km[30];
int currentIndex;
int tableLength;
double ER=6371;
double distL(double lat1,double lon1,double lat2,double lon2){
  lat1/=180/M_PI;
  lon1/=180/M_PI;
  lat2/=180/M_PI;
  lon2/=180/M_PI;
  double a=sin((lat2-lat1)/2)*sin((lat2-lat1)/2)+cos(lat1)*cos(lat2)*sin((lon2-lon1)/2)*sin((lon2-lon1)/2);
  double c=2*atan2(sqrt(a),sqrt(1-a));
  return ER*c;
}
double bearing(double lat1,double lon1,double lat2,double lon2){
  lat1/=180/M_PI;
  lon1/=180/M_PI;
  lat2/=180/M_PI;
  lon2/=180/M_PI;
  double y=sin(lon2-lon1)*cos(lat2);
  double x=cos(lat1)*sin(lat2)-sin(lat1)*cos(lat2)*cos(lon2-lon1);
  return atan2(y,x);
}
double findKMLL(double lat1,double lon1,int currentIndex){
  int i,j;
  double dist[tableLength];
  for(i=0;i<tableLength;i++){
    dist[i]=distL(lat[i],lon[i],lat1,lon1);
    printf("dist = %.4f\n",sqrt(dist[i]));
    if(i>1){
      if((dist[i-2]>dist[i-1]&&dist[i]>dist[i-1]))
        if(dist[i-2]>dist[i]){
          rr=distL(lat[i-1],lon[i-1],lat1,lon1)*sin(bearing(lat[i],lon[i],lat[i-1],lon[i-1])-bearing(lat1,lon1,lat[i-1],lon[i-1]));
          return dist[i-1]*cos(bearing(lat[i],lon[i],lat[i-1],lon[i-1])-bearing(lat1,lon1,lat[i-1],lon[i-1]))+km[i-1];
          }
        else{
          rr=distL(lat[i-2],lon[i-2],lat1,lon1)*sin(bearing(lat[i-2],lon[i-2],lat[i-1],lon[i-1])-bearing(lat[i-2],lon[i-2],lat1,lon1));
          return dist[i-2]*cos(bearing(lat[i-2],lon[i-2],lat[i-1],lon[i-1])-bearing(lat[i-2],lon[i-2],lat1,lon1))+km[i-2];
          }
      if(i==tableLength-1&&dist[i]<dist[i-1]){
        
          rr=distL(lat[i-1],lon[i-1],lat1,lon1)*sin(bearing(lat[i],lon[i],lat[i-1],lon[i-1])-bearing(lat1,lon1,lat[i-1],lon[i-1]));
        return dist[i-1]*cos(bearing(lat[i],lon[i],lat[i-1],lon[i-1])-bearing(lat1,lon1,lat[i-1],lon[i-1]))+km[i-1];
        
      }
    }
  }
    i=1;
          rr=distL(lat[i-1],lon[i-1],lat1,lon1)*sin(bearing(lat[i],lon[i],lat[i-1],lon[i-1])-bearing(lat1,lon1,lat[i-1],lon[i-1]));
    return dist[0]*cos(bearing(lat[0],lon[0],lat[1],lon[1])-bearing(lat[0],lon[0],lat1,lon1))+km[0];
}
void getSDsettingout(FILE* n1){
  int i,j;
  char a[100];
  for(i=0;!feof(n1);i++){
    fscanf(n1,"%[^\n]%*c",a);
    //printf("%s\n",a);
    char i1[10],i2[10],i3[10];
    sscanf(a,"%s %s %s",i1,i2,i3);
    lat[i]=strtod(i1,NULL);
    lon[i]=strtod(i2,NULL);
    km[i]=strtod(i3,NULL);
  }
  tableLength=i;
}
int main(){
    FILE *n1=fopen("testroute.txt","r");
    getSDsettingout(n1);
    int i=0;
    for(i=0;i<tableLength;i++)
    printf("%.5f %.5f %.2f\n",lat[i],lon[i],km[i]);
    printf("%d\n",tableLength);
    while(1){
      double lt,lot,x,y,z;
      scanf("%lf %lf",&lt,&lot);
      printf("dist = %.4f\n",findKMLL(lt,lot,0));
    }
    return 0;
}
