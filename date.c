#include <stdio.h>
#include <stdlib.h>
#include <string.h>
char date[9],time[7],dateC[9],timeC[7];
int md[12]={31,28,31,30,31,30,31,31,30,31,30,31};
void ZoneChange(){
    int day=(date[0]-48)*10+date[1]-48;
    int month=(date[2]-48)*10+date[3]-48;
    int year=(date[4]-48)*1000+(date[5]-48)*100+(date[6]-48)*10+date[7]-48;
    int hour=(time[0]-48)*10+time[1]-48;
    int minute=(time[2]-48)*10+time[3]-48;
    int second=(time[4]-48)*10+time[5]-48;
    hour+=7;
    if(hour>=24){
        hour-=24;
        day++;
        if(year%4==0&&!(year%400!=0&&year%100==0))
            md[1]=29;
        else
            md[1]=28;
        if(day>md[month-1]){
            day=1;
            month++;
            if(month>12){
                month=1;
                year++;
            }
        }
    }
    dateC[0]=day/10+48;
    dateC[1]=day-(day/10)*10+48;
    dateC[2]=month/10+48;
    dateC[3]=month-(month/10)*10+48;
    dateC[4]=year/1000+48;
    dateC[5]=(year-(year/1000)*1000)/100+48;
    dateC[6]=(year-(year/100)*100)/10+48;
    printf("%d %d %d %d %d\n",year,(year/10)*10,(year/100)*100,(year/1000)*1000,dateC[6]);
    dateC[7]=(year-(year/10)*10)+48;
    dateC[8]='\0';
    timeC[0]=hour/10+48;
    timeC[1]=hour-(hour/10)*10+48;
    timeC[2]=minute/10+48;
    timeC[3]=minute-(minute/10)*10+48;
    timeC[4]=second/10+48;
    timeC[5]=second-(second/10)*10+48;
    printf("\n%s %s\n",dateC,timeC);
}
int main(){
    while(1){
        scanf("%s %s",date,time);
        ZoneChange();
    }
}