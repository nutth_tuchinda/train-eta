const int PIRpin = 13;
const int ledpin = 12;

int limit;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial3.begin(9600);
  pinMode ( PIRpin, INPUT_PULLUP );
  pinMode ( ledpin , OUTPUT );
}
void collectapiframe (byte *collect){       // Output : API frame and lenght
  //Serial3.flush();
  bool complete = false ;
  while(!complete){
    Serial3.flush();
    if(Serial3.available()>10) { 
     byte inbyte = Serial3.read();
     delay(1);
     byte inbyte1 = Serial3.read();
     delay(1);
      if(inbyte == 0x7e && inbyte1==0x00){ 
        byte inbyte2 = Serial3.read();
        delay(1);
        limit = (int)inbyte1 + (int)inbyte2;
        collect[0] = inbyte ;
        collect[1] = inbyte1  ;
        collect[2] = inbyte2  ;
      }
      for(int i = 3 ; i <= limit+3 ; i++){         
        collect[i] = Serial3.read();
        delay(1);
      }
      for(int i = 0 ; i <= limit + 3 ; i++){
        Serial.print(collect[i],HEX);
        Serial.print(",");                                      
      }
      Serial.print("    ");
      complete = true;
    }
  }
}
void TransmitRequest(byte *data,int framelength,byte *address,int addresslength,int frameid){      // Frame type 0x10
  int framelength1 = framelength + 14;
  long checksum = 0;
  Serial3.write(0x7E);
  Serial3.write(0x00);                         //MSB Length
  Serial3.write(framelength1);                  //LSB Length
  Serial3.write(0x10);                         //Frametype 0x10
  Serial3.write(frameid);
  for(int i = 0 ; i < addresslength ; i++){        // Address 64 bit
    Serial3.write(address[i]);
    checksum = checksum + address[i];
  }
  Serial3.write(0xFF);                        // Address 16 bit Set to 0xFFFE if unknown
  Serial3.write(0xFE);
  Serial3.write(0x00);                        // Broadcast radius
  Serial3.write(0x00);                        // Options
  for(int i = 0 ; i < framelength ; i++){     // RF data
    Serial3.write(data[i]);
    checksum = checksum + data[i];
  }
  checksum = checksum + 0x10 + frameid + 0xFF + 0xFE ;
  checksum = 0xFF - (checksum & 0xFF);
  Serial3.write(checksum);                    // Checksum byte
} 
bool confirmframe(byte *collect,int frameid){
  Serial.print(collect[4],HEX); Serial.print(frameid,HEX);
  bool compare;
  if ( collect[4] == frameid && collect[8] == 0 ){
    compare = true;
  }
  else{
    compare = false;
  } 
  return compare;
}



void loop() {
  // put your main code here, to run repeatedly:
  byte collect[] = {} ;
  int limit;
  //float total = 0 , success = 0  , fail=0;
  byte data[] = { 'Z', 'I', 'G', 'B', 'E', 'E', 'T', 'E', 'S', 'T'};    //  Data to send to Target
  byte address[] = { 0x00, 0x13, 0xA2, 0x00, 0x40, 0xDA, 0x50, 0xF8};   //  Set Target address 
  for(int i = 1 ; i < 255 ; i++){
    int frameid = i;
    TransmitRequest(data,sizeof(data), address,sizeof(address),frameid);
//    collectapiframe(collect);
//      if (confirmframe(collect,frameid)){
//        success++; 
//        total++;
//      }
//      if(!confirmframe(collect,frameid)){
//        fail++;
//        total++;
//      }
//    float successrate = success / total * 100 ;
//    Serial.print("    Total : "); Serial.print((int)total); Serial.print("     Success : "); Serial.print((int)success); Serial.print("       Failure : "); Serial.print((int)fail); Serial.print("      Success Rate : ");Serial.print(successrate,2); Serial.println(" % ");
    delay(1000);
  }
}
